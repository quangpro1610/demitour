jQuery(document).ready(function($) {	
	if($('.sliders').length){
		$('.sliders').slick({
			infinite: false,
			autoplay: false,
			slidesToShow: 4,
			arrows: true
		});
	}
	if($('.testimonial-slide').length){
		$('.testimonial-slide').slick({
			slidesToShow: 1,
			autoplay: false,
			arrows: false,
			dots: true
		});
	}
	$('.show-calendar').on('click', function(e) {
		e.preventDefault();
		$(this).parent().pignoseCalendar({
			modal: false,
		 	buttons: false,
		 	disabledDates: [
				'2019-11-03',
				'2019-11-04',
				'2019-11-06',
				'2019-11-07',
				'2019-11-29',
				'2019-11-30'
			]
		});
	});
	$('.input-number-increment').click(function() {
	  	var $input = $(this).parents('.input-number-group').find('.input-number');
	  	var val = parseInt($input.val(), 10);
	  	$input.val(val + 1);
	  	if($input.val() > 0){
	  		$(this).parent('.input-group-button').siblings('.input-group-button').removeClass('disabledbutton');
	  	}
	});

	$('.input-number-decrement').click(function() {
	  	var $input = $(this).parents('.input-number-group').find('.input-number');
	  	var val = parseInt($input.val(), 10);
	  	$input.val(val - 1);
	  	if($input.val() == 0){
	  		$(this).parent('.input-group-button').addClass('disabledbutton');
	  	}
	})
	$('.btn-next').on('click', function(e) {
		e.preventDefault();
		var form_step = $(this).data('form-step');
		$(this).parents('.content-fm').siblings('.content-fm'+form_step).addClass('active');
		$(this).parents('.content-fm').removeClass('active');
		$(this).parents('.rsbooking-form').find('.rs'+form_step).addClass('active');
	});
	$('#popup-success .close').click(function(e) {
		e.preventDefault();
		$(this).parents('#popup-success').hide();
	});
});
